package net.pl3x.bukkit.teleports.command;

import net.pl3x.bukkit.teleports.Pl3xTeleports;
import net.pl3x.bukkit.teleports.configuration.Lang;
import net.pl3x.bukkit.teleports.task.TeleportSounds;
import org.bukkit.Location;
import org.bukkit.command.Command;
import org.bukkit.command.CommandSender;
import org.bukkit.command.TabExecutor;
import org.bukkit.entity.Player;

import java.util.HashMap;
import java.util.List;
import java.util.UUID;

public class CmdBack implements TabExecutor {
    private final Pl3xTeleports plugin;
    private static final HashMap<UUID, Location> backdb = new HashMap<>();

    public CmdBack(Pl3xTeleports plugin) {
        this.plugin = plugin;
    }

    @Override
    public List<String> onTabComplete(CommandSender sender, Command cmd, String label, String[] args) {
        return null;
    }

    @Override
    public boolean onCommand(CommandSender sender, Command cmd, String label, String[] args) {
        if (!(sender instanceof Player)) {
            Lang.send(sender, Lang.PLAYER_COMMAND);
            return true;
        }

        if (!sender.hasPermission("command.back")) {
            Lang.send(sender, Lang.COMMAND_NO_PERMISSION);
            return true;
        }

        Player player = (Player) sender;
        Location back = getPreviousLocation(player);

        if (back == null) {
            Lang.send(sender, Lang.NO_BACK_LOCATION);
            return true;
        }

        new TeleportSounds(back, player.getLocation())
                .runTaskLater(plugin, 1);

        player.teleport(back);

        Lang.send(sender, Lang.TELEPORTING_BACK);
        return true;
    }

    private static Location getPreviousLocation(Player player) {
        return backdb.get(player.getUniqueId());
    }

    public static void setPreviousLocation(Player player, Location location) {
        if (location == null) {
            backdb.remove(player.getUniqueId());
            return;
        }
        backdb.put(player.getUniqueId(), location);
    }

    public static void clearBackLocations() {
        backdb.clear();
    }
}
