package net.pl3x.bukkit.teleports.command;

import net.pl3x.bukkit.teleports.Pl3xTeleports;
import net.pl3x.bukkit.teleports.configuration.Lang;
import net.pl3x.bukkit.teleports.configuration.PlayerConfig;
import net.pl3x.bukkit.teleports.request.TpaHereRequest;
import org.bukkit.Bukkit;
import org.bukkit.command.Command;
import org.bukkit.command.CommandSender;
import org.bukkit.command.TabExecutor;
import org.bukkit.entity.Player;

import java.util.List;
import java.util.UUID;

public class CmdTeleportRequestAll implements TabExecutor {
    private final Pl3xTeleports plugin;

    public CmdTeleportRequestAll(Pl3xTeleports plugin) {
        this.plugin = plugin;
    }

    @Override
    public List<String> onTabComplete(CommandSender sender, Command cmd, String label, String[] args) {
        return null;
    }

    @Override
    public boolean onCommand(CommandSender sender, Command cmd, String label, String[] args) {
        if (!(sender instanceof Player)) {
            Lang.send(sender, Lang.PLAYER_COMMAND);
            return true;
        }

        if (!sender.hasPermission("command.teleportrequestall")) {
            Lang.send(sender, Lang.COMMAND_NO_PERMISSION);
            return true;
        }

        UUID senderUUID = ((Player) sender).getUniqueId();
        for (Player target : Bukkit.getOnlinePlayers()) {
            if (target.getUniqueId().equals(senderUUID)) {
                continue;
            }

            // check for pending requests
            PlayerConfig targetConfig = PlayerConfig.getConfig(plugin, target);
            if (targetConfig.getRequest() != null) {
                continue; // skip
            }

            // check for toggles and overrides
            if (target.hasPermission("command.teleporttoggle") &&
                    !sender.hasPermission("command.teleport.override") &&
                    !targetConfig.allowTeleports()) {
                continue;
            }

            // Create new request
            targetConfig.setRequest(new TpaHereRequest(plugin, (Player) sender, target));
        }

        Lang.send(sender, Lang.TELEPORT_REQUESTALL_REQUESTER);
        return true;
    }
}
