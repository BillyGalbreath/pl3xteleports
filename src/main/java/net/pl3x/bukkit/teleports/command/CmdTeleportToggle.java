package net.pl3x.bukkit.teleports.command;

import net.pl3x.bukkit.teleports.Pl3xTeleports;
import net.pl3x.bukkit.teleports.configuration.Lang;
import net.pl3x.bukkit.teleports.configuration.PlayerConfig;
import org.apache.commons.lang.BooleanUtils;
import org.bukkit.command.Command;
import org.bukkit.command.CommandSender;
import org.bukkit.command.TabExecutor;
import org.bukkit.entity.Player;

import java.util.List;

public class CmdTeleportToggle implements TabExecutor {
    private final Pl3xTeleports plugin;

    public CmdTeleportToggle(Pl3xTeleports plugin) {
        this.plugin = plugin;
    }

    @Override
    public List<String> onTabComplete(CommandSender sender, Command cmd, String label, String[] args) {
        return null;
    }

    @Override
    public boolean onCommand(CommandSender sender, Command cmd, String label, String[] args) {
        if (!(sender instanceof Player)) {
            Lang.send(sender, Lang.PLAYER_COMMAND);
            return true;
        }

        if (!sender.hasPermission("command.teleporttoggle")) {
            Lang.send(sender, Lang.COMMAND_NO_PERMISSION);
            return true;
        }

        PlayerConfig config = PlayerConfig.getConfig(plugin, (Player) sender);
        config.setAllowTeleports(!config.allowTeleports());

        Lang.send(sender, Lang.TELEPORT_TOGGLE_SET
                .replace("{toggle}", BooleanUtils.toStringOnOff(config.allowTeleports())));
        return true;
    }
}
