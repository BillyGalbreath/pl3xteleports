package net.pl3x.bukkit.teleports.configuration;

import net.pl3x.bukkit.teleports.Pl3xTeleports;
import net.pl3x.bukkit.teleports.request.Request;
import org.bukkit.configuration.file.YamlConfiguration;
import org.bukkit.entity.Player;

import java.io.File;
import java.util.HashMap;
import java.util.Map;

public class PlayerConfig extends YamlConfiguration {
    private static final Map<Player, PlayerConfig> configs = new HashMap<>();

    public static PlayerConfig getConfig(Pl3xTeleports plugin, Player player) {
        synchronized (configs) {
            return configs.computeIfAbsent(player, k -> new PlayerConfig(plugin, player));
        }
    }

    public static void remove(Player player) {
        configs.remove(player);
    }

    public static void removeAll() {
        synchronized (configs) {
            configs.clear();
        }
    }

    private File file = null;
    private final Object saveLock = new Object();
    private Request request;

    private PlayerConfig(Pl3xTeleports plugin, Player player) {
        super();
        file = new File(plugin.getDataFolder(),
                "userdata" + File.separator + player.getUniqueId() + ".yml");
        reload();
    }

    private void reload() {
        synchronized (saveLock) {
            try {
                load(file);
            } catch (Exception ignore) {
            }
        }
    }

    private void save() {
        synchronized (saveLock) {
            try {
                save(file);
            } catch (Exception ignore) {
            }
        }
    }

    public boolean allowTeleports() {
        return getBoolean("allow-teleports", true);
    }

    public void setAllowTeleports(boolean allowTeleports) {
        set("allow-teleports", allowTeleports);
        save();
    }

    public Request getRequest() {
        return request;
    }

    public void setRequest(Request request) {
        this.request = request;
    }
}
