package net.pl3x.bukkit.teleports;

import net.pl3x.bukkit.teleports.command.CmdBack;
import net.pl3x.bukkit.teleports.command.CmdJump;
import net.pl3x.bukkit.teleports.command.CmdPl3xTeleports;
import net.pl3x.bukkit.teleports.command.CmdTeleportAccept;
import net.pl3x.bukkit.teleports.command.CmdTeleportDeny;
import net.pl3x.bukkit.teleports.command.CmdTeleportRequest;
import net.pl3x.bukkit.teleports.command.CmdTeleportRequestAll;
import net.pl3x.bukkit.teleports.command.CmdTeleportRequestHere;
import net.pl3x.bukkit.teleports.command.CmdTeleportToggle;
import net.pl3x.bukkit.teleports.command.CmdTop;
import net.pl3x.bukkit.teleports.configuration.Config;
import net.pl3x.bukkit.teleports.configuration.Lang;
import net.pl3x.bukkit.teleports.configuration.PlayerConfig;
import net.pl3x.bukkit.teleports.listener.PlayerListener;
import org.bukkit.Bukkit;
import org.bukkit.plugin.java.JavaPlugin;

public class Pl3xTeleports extends JavaPlugin {
    private final Logger logger;

    public Pl3xTeleports() {
        this.logger = new Logger(this);
    }

    public Logger getLog() {
        return logger;
    }

    @Override
    public void onEnable() {
        Config.reload(this);
        Lang.reload(this);

        Bukkit.getPluginManager().registerEvents(new PlayerListener(), this);

        getCommand("back").setExecutor(new CmdBack(this));
        getCommand("pl3xteleports").setExecutor(new CmdPl3xTeleports(this));
        getCommand("jump").setExecutor(new CmdJump(this));
        getCommand("teleportaccept").setExecutor(new CmdTeleportAccept(this));
        getCommand("teleportdeny").setExecutor(new CmdTeleportDeny(this));
        getCommand("teleportrequest").setExecutor(new CmdTeleportRequest(this));
        getCommand("teleportrequestall").setExecutor(new CmdTeleportRequestAll(this));
        getCommand("teleportrequesthere").setExecutor(new CmdTeleportRequestHere(this));
        getCommand("teleporttoggle").setExecutor(new CmdTeleportToggle(this));
        getCommand("top").setExecutor(new CmdTop(this));

        getLog().info(getName() + " v" + getDescription().getVersion() + " enabled!");
    }

    @Override
    public void onDisable() {
        CmdBack.clearBackLocations();
        PlayerConfig.removeAll();

        getLog().info(getName() + " disabled.");
    }
}
