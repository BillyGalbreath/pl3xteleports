package net.pl3x.bukkit.teleports.request;

import net.pl3x.bukkit.teleports.Pl3xTeleports;
import net.pl3x.bukkit.teleports.configuration.Lang;
import org.bukkit.entity.Player;

public class TpaHereRequest extends Request {
    public TpaHereRequest(Pl3xTeleports plugin, Player requester, Player target) {
        super(plugin, requester, target);
        Lang.send(target, Lang.TELEPORT_REQUESTHERE_TARGET
                .replace("{requester}", requester.getName()));
    }

    @Override
    protected void teleport() {
        if (!getTarget().isOnline() || !getRequester().isOnline()) {
            cancel();
            return;
        }
        playTeleportSounds();
        getTarget().teleport(getRequester());
    }
}
