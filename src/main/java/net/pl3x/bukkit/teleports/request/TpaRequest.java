package net.pl3x.bukkit.teleports.request;

import net.pl3x.bukkit.teleports.Pl3xTeleports;
import net.pl3x.bukkit.teleports.configuration.Lang;
import org.bukkit.entity.Player;

public class TpaRequest extends Request {
    public TpaRequest(Pl3xTeleports plugin, Player requester, Player target) {
        super(plugin, requester, target);
        Lang.send(target, Lang.TELEPORT_REQUEST_TARGET
                .replace("{requester}", requester.getName()));
    }

    @Override
    protected void teleport() {
        if (!getTarget().isOnline() || !getRequester().isOnline()) {
            cancel();
            return;
        }
        playTeleportSounds();
        getRequester().teleport(getTarget());
    }
}
